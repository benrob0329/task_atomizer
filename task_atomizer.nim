import os, system, std/strutils, std/strformat, std/sequtils, std/math, std/wordwrap
import illwill

type
  TodoItem = object
    text: string
    done: bool
    indent: Natural
  
  MenuSelection = enum
    Copy, Paste, Open, Save, Quit

  FileSelection = enum
    Open, Cancel

const
  limit_max_indent = 25

var
  initial_startup = true

  todos: seq[TodoItem] = @[TodoItem(text: "", done: false, indent: 0)]
  current_todo: Natural = 0

  show_menu = false
  current_menu: MenuSelection = Copy
  clipboard = TodoItem(text: "", done: false, indent: 0)

  current_file_button: FileSelection = Open
  file_entree: string = ""
  show_open = false
  show_save = false

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc readTodoFile(file_name: string): seq[TodoItem] =
  result = @[]
  var
    f: File
    l: string

  if open(f, file_name):
    try:
      while true:
        let read = readLine(f, l)
        if read:
          let split_line = l.rsplit({':'})
          result.add(TodoItem(text: split_line[0], done: split_line[1].parseBool(), indent: split_line[2].parseInt()))
        else:
          break
    except CatchableError:
      raise
      exitProc()
    finally:
      close(f)
  else:
    exitProc()

proc writeTodoFile(file_name: string, todo_seq: seq[TodoItem]) =
  var f: File
  
  if open(f, file_name, fmWrite):
    try:
      for i, todo in todo_seq:
        f.writeLine(todo.text, ":", todo.done, ":", todo.indent)
    except CatchableError:
      raise
      exitProc()
    finally:
      close(f)
  else:
    exitProc()

proc handleTodoKeys(key: Key) =
  case key
  # Skip over colons as we don't handle escaping them in the file yet
  of Key.Space..Key.Nine, Key.SemiColon..Key.Tilde:
      todos[current_todo].text.add(key.ord.char)
  of Key.Backspace:
    if todos[current_todo].text.len() > 1:
      todos[current_todo].text = todos[current_todo].text[0..^2]
    else:
      todos[current_todo].text = ""
  of Key.Enter:
    todos.insert(TodoItem(text: "", indent: todos[current_todo].indent, done: false), current_todo + 1)
    inc current_todo
  of Key.Up:
    if current_todo == 0:
      discard
    else:
      dec current_todo
  of Key.Down:
    if current_todo == (todos.len - 1):
      discard
    else:
      inc current_todo
  of Key.Left:
    if todos[current_todo].indent > 0:
      dec todos[current_todo].indent
    else:
      discard
  of Key.Right:
    if todos[current_todo].indent <= limit_max_indent:
      inc todos[current_todo].indent
    else:
      discard
  of Key.Tab:
    todos[current_todo].done = not todos[current_todo].done
  of Key.Delete:
    if todos.len == 1:
      discard
    else:
      todos.delete(current_todo)
      if current_todo >= todos.len - 1:
        dec current_todo
  of Key.PageUp:
    if current_todo == 0:
      discard
    else:
      let temp = todos[current_todo]
      todos.delete(current_todo)
      dec current_todo
      todos.insert(temp, current_todo)
  of Key.PageDown:
    if current_todo >= todos.len - 1:
      discard
    else:
      let temp = todos[current_todo]
      todos.delete(current_todo)
      inc current_todo
      todos.insert(temp, current_todo)
  else:
    discard

proc handleMenuKeys(key: Key) =
  case key
  of Key.Up:
    if current_menu == Copy:
      current_menu = Quit
    else:
      dec current_menu
  of Key.Down:
    if current_menu == Quit:
      current_menu = Copy
    else:
      inc current_menu
  of Key.Enter:
    case current_menu
    of Copy:
      clipboard = todos[current_todo]
      show_menu = false
    of Paste:
      todos[current_todo] = clipboard
      show_menu = false
    of Open:
      show_open = true
    of Save:
      show_save = true
    of Quit:
      exitProc()
  else:
    discard

proc handleOpenKeys(key: Key) =
  case key
  of Key.Zero .. Key.Nine, Key.ShiftA .. Key.ShiftZ, Key.A .. Key.Z, Key.Space, Key.Dot, Key.Slash, Key.BackSlash:
    file_entree.add(key.ord.char)
  of Key.Backspace:
    if file_entree.len() < 2:
      file_entree = ""
    else:
      file_entree = file_entree[0..^2]
  of Key.Left, Key.Right:
    case current_file_button
    of Open:
      current_file_button = Cancel
    of Cancel:
      current_file_button = Open
  of Key.Enter:
    if current_file_button == Open:
      todos = readTodoFile(file_entree)
    show_open = false
    show_menu = false
  else:
    discard

proc handleSaveKeys(key: Key) =
  case key
  of Key.Zero .. Key.Nine, Key.ShiftA .. Key.ShiftZ, Key.A .. Key.Z, Key.Space, Key.Dot, Key.Slash, Key.BackSlash:
    file_entree.add(key.ord.char)
  of Key.Backspace:
    if file_entree.len() < 2:
      file_entree = ""
    else:
      file_entree = file_entree[0..^2]
  of Key.Left, Key.Right:
    case current_file_button
    of Open:
      current_file_button = Cancel
    of Cancel:
      current_file_button = Open
  of Key.Enter:
    if current_file_button == Open:
      writeTodoFile(file_entree, todos)
    show_save = false
    show_menu = false
  else:
    discard

illwillInit(fullscreen=true)
setControlCHook(exitProc)
hideCursor()

while true:
  let
    screen_width: Natural = terminalWidth()
    screen_height: Natural = terminalHeight()

    center_width = floor(screen_width / 2).int
    center_height = floor(screen_height / 2).int

  var tb = newTerminalBuffer(screen_width, screen_height)
  
  if (screen_width < 80) or (screen_height < 15):
    tb.write(0, 0, "Your terminal is too small.")
    tb.write(0, 1, "Please resize your terminal.")
    tb.display()
    sleep(100)
    continue

  var key = getKey()
  case key
  of Key.None:
    if not initial_startup:
      sleep(20)
      continue
    else:
      discard
  of Key.Escape:
    show_menu = not show_menu
  else:
    discard

  if show_open:
    handleOpenKeys(key)
  elif show_save:
    handleSaveKeys(key)
  elif show_menu:
    handleMenuKeys(key)
  else:
    handleTodoKeys(key)
  
  # Write todos
  for i, t in todos:
    let done = if t.done: "[√] " else: "[ ] "
    let indent = t.indent * 2

    if i == current_todo:
      tb.setForegroundColor(fgBlack)
      tb.setBackgroundColor(bgWhite)
    else:
      tb.resetAttributes()
    
    tb.write(indent, i, " ".repeat(screen_width - indent))
    tb.write(indent, i, done, t.text)

  # Draw Menu
  if show_menu:
    tb.setForegroundColor(fgWhite)
    tb.setBackgroundColor(bgBlack)

    tb.fill(4, 2, screen_width - 4, screen_height - 2, " ")
    tb.drawRect(4, 2, screen_width - 4, screen_height - 2, doubleStyle = true)

    # Buttons
    tb.write(
      center_width - 4,
      center_height - 4,
      if current_menu == Copy: fgBlack else: fgWhite,
      if current_menu == Copy: bgWhite else: bgBlack,
      "Copy".center(8)
    )
    tb.write(
      center_width - 4,
      center_height - 2,
      if current_menu == Paste: fgBlack else: fgWhite,
      if current_menu == Paste: bgWhite else: bgBlack,
      "Paste".center(8)
    )
    tb.write(
      center_width - 4,
      center_height,
      if current_menu == Open: fgBlack else: fgWhite,
      if current_menu == Open: bgWhite else: bgBlack,
      "Open".center(8)
    )
    tb.write(
      center_width - 4,
      center_height + 2,
      if current_menu == Save: fgBlack else: fgWhite,
      if current_menu == Save: bgWhite else: bgBlack,
      "Save".center(8)
    )
    tb.write(
      center_width - 4,
      center_height + 4,
      if current_menu == Quit: fgBlack else: fgWhite,
      if current_menu == Quit: bgWhite else: bgBlack,
      "Quit".center(8)
    )

  if show_open or show_save:
    tb.setForegroundColor(fgWhite)
    tb.setBackgroundColor(bgBlack)

    tb.fill(center_width - 10, center_height - 2, center_width + 10, center_height + 3, " ")
    tb.drawRect(center_width - 10, center_height - 2, center_width + 10, center_height + 3, doubleStyle = true)

    # Entree
    tb.setBackgroundColor(bgWhite)
    tb.setForegroundColor(fgBlack)
    tb.write(center_width - 8, center_height, " ".repeat(16))
    tb.write(center_width - 8, center_height, file_entree)

    # Open/Save Button
    let open_text = if show_open: "Open" else: "Save"    
    tb.write(
      center_width - 8,
      center_height + 2,
      if current_file_button == Open: fgBlack else: fgWhite,
      if current_file_button == Open: bgWhite else: bgBlack,
      open_text.center(6)
    )

    # Cancel Button
    tb.write(
      center_width + 3,
      center_height + 2,
      if current_file_button == Cancel: fgBlack else: fgWhite,
      if current_file_button == Cancel: bgWhite else: bgBlack,
      "Cancel".center(6)
    )

  tb.display()
  sleep(20)
